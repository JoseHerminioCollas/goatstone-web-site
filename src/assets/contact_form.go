package hellogoat

import (
	"fmt"
	"net/http"
	"appengine"
	"appengine/mail"
	"time"
	"appengine/datastore"
)

func init() {
	http.HandleFunc("/contact", sign)
}

type Greeting struct {
	UserName    string
	UserEmail   string
	UserSubject string
	UserMessage string
	Date        time.Time
}

const confirmMessage = `
Email from web site form:
name : %[1]s
email : %[2]s
subject : %[3]s
user_message :%[4]s
`

func sign(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	user_name := r.FormValue("user_name")
	user_email := r.FormValue("user_email")
	user_subject := r.FormValue("user_subject")
	user_message := r.FormValue("user_message")
	msg := &mail.Message{
		Sender:  "FORM MAIL <jose.collas@goatstone.com>",
		To:      []string{"jose.collas@goatstone.com"},
		Subject: "* Mail from Site!" ,
		Body:    fmt.Sprintf(confirmMessage, user_name, user_email, user_subject, user_message),
	}
	if err := mail.Send(c, msg); err != nil {
		c.Errorf("Couldn't send email: %v", err)
	}
	g := Greeting{
		UserName: user_name,
		UserEmail: user_email,
		UserSubject: user_subject,
		UserMessage: user_message,
		Date: time.Now(),
	}
	// We set the same parent key on every Greeting entity to ensure each Greeting
	// is in the same entity group. Queries across the single entity group
	// will be consistent. However, the write rate to a single entity group
	// should be limited to ~1/second.
	key := datastore.NewIncompleteKey(c, "Contacts", guestbookKey(c))
	_, err := datastore.Put(c, key, &g)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//c.Infof(" hello go: %v", user_name, "XXXXXXX")
}

// guestbookKey returns the key used for all guestbook entries.
func guestbookKey(c appengine.Context) *datastore.Key {
	// The string "default_guestbook" here could be varied to have multiple guestbooks.
	return datastore.NewKey(c, "ContactForm", "default_contact", 0, nil)
}
