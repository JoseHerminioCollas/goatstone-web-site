import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'todo-header',
  template: `
  <md-toolbar>
    <h3>{{config.title}}</h3>
    <div class="icon-container">
      <button (click)=openDialog(ADD) md-mini-fab>
        <md-icon>email</md-icon>
      </button>
    </div>
  </md-toolbar>
`,
  styleUrls: [`./style.css`]
})

export class TodoHeader {
  @Input() config: any
  @Output() emitOpenDialog: EventEmitter<string> = new EventEmitter()
  readonly ADD = 'add'
  readonly INFO = 'info'
  constructor () {}
  openDialog (whichDialog: string) {
    this.emitOpenDialog.emit(whichDialog)
  }
}
