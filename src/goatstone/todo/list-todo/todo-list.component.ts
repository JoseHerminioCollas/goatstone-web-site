import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Todo } from 'goatstone/todo/models/todo'
import { Observable } from 'rxjs/Observable'

@Component({
  selector: 'todo-list',
  template: `
<md-card *ngFor="let todo of todos; index as i">
  <md-card-title>
    {{todo.name}}
  </md-card-title>
  <md-card-content>
    {{todo.description}}    
  </md-card-content>
  <md-card-footer *ngIf="todo.link.url !== '' ">    
      <button (click)=follow(todo.link.url) md-mini-fab>
        <md-icon>launch</md-icon>
      </button>
  </md-card-footer>
</md-card>
`,
  styles: [`
    :host {
      display: flex;
      flex-direction: column;
      max-width: 700px;
      width: 90%;
    }
    :host /deep/ md-card-title {
      color: rgb(40, 170, 200);
      background: none;
      font-weight: 900;
      margin-bottom: 6px;
    }
    :host /deep/ md-card-content {
      color: #333;
      box-shadow: 8px 9px 10px #ccc;
      background: #eef;
      padding: 12px 12px 16px 12px;
    }
    :host /deep/ md-card-footer {
      color: #333;
      padding: 12px;
      width: 80%;
      text-align: right;
    }
    `
  ]
})

export class TodoList {
  @Input() todos: any
  hasLink = false
  constructor () {}
  follow (a: any) {
    window.open(a)
  }
}
