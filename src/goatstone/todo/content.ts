export let content = [
    {
    name: `
Welcome to Goatstone
    `,
    description: ` 
Goatstone is primarily, but not exclusively, Jose Collas. 
I have been involved in web development since the days of Netscape. 
I have worked with a number of web technologies. 
Currently I have been focusing on JavaScript, CSS, NodeJS, Angular 2, React, Rx and Material Design.
    `,
    link: {url: '', title: ''}
  },
  {
    name: `
Octogoat, a search engine for electronic parts
    `,
    description: ` 
I have recently launched a search engine for electronics parts. 
This search engine is powered by the Octopart.com API. 
It is developed with Angular 2 and Material Design components. 
A user can type in a search term, initiate a search and see results displayed.    
    `,
    link: {url: '//octogoat.com', title: 'Octogoat.com'}
  },
  {
    name: `
Angular versus React
    `,
    description: ` 
I have developed Todo applicatons with React and Angular in orer to compare the two.
    `,
    link: {url: '/blog/angular-vs-react.html', title: 'Angular vs. React'}
  },
  {
    name: `
SVG Samples
    `,
    description: ` 
I have developed a series of SVG graphics using the JavaScript libraries: 
Snap.svg (http://snapsvg.io ) and StampIt (https://github.com/ericelliott/stampit). This project explores various strategies for implementing SVG graphics, especially animations.
    `,
    link: {url: '//svg.goatstone.com', title: 'SVG Samples'}
  },
  {
    name: `
AlphaCronke    
    `,
    description: ` 
My favorite JavaScript libray is D3js (d3js.org). "AlphaCronke" is the latest work I have done with this library. The application consists of text from the story, Dickory Cronke by Daniel Defoe and a widget that enables the user to select words by alphabetical range. With this widget the user can highlight the selected words in the text. D3js is the only library used. 
    `,
    link: {url: 'http://alphacronke.goatstone.com/', title: ''}
  },
  {
    name: `
Ameb, a game
    `,
    description: ` 
Ameb was my entry into the JS13kGames JavaScript competition. 
The player of Ameb navigates the character, Ameb, to catch food. Ameb must eat food in order to accumulate health points and stay alive. 
       `,
    link: {url: 'http://ameb.goatstone.com/', title: ''}
  },
  {
    name: `
Android
    `,
    description: ` 
I have been working with the sensors on Android and have set up an open source project, AndroidSensorFusion on Github. The Java class com.goatstone.util.SensorFusion gets sensor information from the Android device concerning its motion and place in space. The code takes these values, fuses them and creates values that are far more usable in any given application.
    `,
    link: {url: 'https://github.com/goatstone/AndroidSensorFusion', title: ''}
  }
]
