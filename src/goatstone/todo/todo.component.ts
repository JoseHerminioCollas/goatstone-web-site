import { Component } from '@angular/core'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Todo } from 'goatstone/todo/models/todo'
import * as todoReduce from 'goatstone/todo/reducers/todo'
import * as todoAction from 'goatstone/todo/actions/todo'
import { DialogService } from 'goatstone/todo/dialog/service'
import { InformationDialog } from 'goatstone/todo/dialog/information'
import { MessageService } from 'goatstone/todo/message/message.service'
import { AddTodoDialog } from 'goatstone/todo/dialog/add-todo'
import { content } from 'goatstone/todo/content'

@Component({
  selector: 'goatstone-todo',
  template: `
  <todo-header
    [config]=headerConfig
    (emitOpenDialog)="openDialog($event)"
  ></todo-header>
  <todo-list
    [todos]=content
  ></todo-list>
`,
styles: [`
:host {
  display: flex;
  flex-direction: column;
  align-items: center;
}
`]
})

export class TodoComponent {
  public headerConfig: any = {title: 'Goatstone'}
  public content: any
  constructor (
      private store: Store<todoReduce.State>, 
      private ds: DialogService, 
      private messageService: MessageService
    ) {
    this.content = content
  }
  public msg (data: any){
    let o = this.messageService.addBookWithObservable(data)
    o.subscribe(x => {
        console.log('sendvvvvv', x)
    })
  }
  public openDialog (which: string) {
    if(which === 'add'){
      this.ds.openDialog(AddTodoDialog)
      .subscribe((data: any) => {
        if(data) {
            this.msg(data)
        }
      })
    }
  }
}
