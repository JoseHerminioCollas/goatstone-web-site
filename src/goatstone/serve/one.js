var express = require('express');
var nodemailer = require('nodemailer');
var router = express.Router();
var app = express();
var path = require('path');
var bodyParser = require('body-parser')
var cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname)));
app.use('/', router);
router.post('/msg', sendMsg);

var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
        user: "",
        pass: ""
    }
});
app.listen(process.env.PORT || 8080);

function sendMsg (req, res){
    console.log(req.body) // name subject message                                                                          
    var mailOptions={
        to : 'jose.collas@goatstone.com',
        subject : req.body.subject,
        text : req.body.email + ':::' + req.body.name + ':::::' + req.body.message
    }
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
            res.end("error");
        }else{
            res.end("sent");
        }
    });
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.write(' - -');
    res.end("Message sent - \n");
}
